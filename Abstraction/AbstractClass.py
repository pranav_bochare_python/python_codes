from abc import ABC,abstractmethod
class Car(ABC):
    def show(self):
        print("Every car has 4 Wheels")
    @abstractmethod
    def speed(self):
        pass
class Maruti(Car):
    def speed(self):
        print("Spped is 1000 km/h")
class Suzuki(Car):
    def speed(self):
        print("speed is 70 km/h")
obj=Maruti()
obj.show()
obj.speed()

obj2=Suzuki()
obj2.show()
obj2.speed()
